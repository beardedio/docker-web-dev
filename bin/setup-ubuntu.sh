#!/usr/bin/env bash
set -euo pipefail

# Colorize me baby
green() { printf '\e[1;32m%b\e[0m\n' "$@"; }
yellow() { printf '\e[1;33m%b\e[0m\n' "$@"; }
red() { printf '\e[1;31m%b\e[0m\n' "$@"; }

# ref: https://askubuntu.com/a/30157/8698
if ! [ "$(id -u)" = 0 ]; then
    red "The script need to be run as root." >&2
    exit 1
fi

if [ "$SUDO_USER" ]; then
    real_user=$SUDO_USER
else
    real_user=$(whoami)
fi

# Check if running on Linux
if [ "$(expr substr "$(uname -s)" 1 5)" != "Linux" ]; then
    red "This script is used to setup docker-web-dev on Linux"
    exit 1
fi

# Check if prerequisites are installed
green "Checking for prerequisites"

if [[ $(command -v jq) == "" ]]; then
    red "jq is required to run this script, please install."
    yellow "To install on Ubuntu run 'sudo apt install jq'"
    exit 1
fi

if [[ $(command -v sponge) == "" ]]; then
    red "sponge is required to run this script, please install."
    yellow "To install on Ubuntu run 'sudo apt install moreutils'"
    exit 1
fi

if [[ $(command -v docker) == "" ]]; then
    red "Docker for Mac is required to run this script, please install."
    yellow "To install on Ubuntu go to https://docs.docker.com/install/linux/docker-ce/ubuntu/"
    exit 1
fi

# Add new loopback address
green "Adding loopback address"
echo "network:
  version: 2
  renderer: networkd
  ethernets:
    lo:
      match:
        name: lo
      addresses: [ 10.254.254.1/32 ]" | sudo tee /etc/netplan/dnsmasq-loopback.yaml > /dev/null
sudo netplan generate
sudo netplan apply

# Diable systemd-resolved dns stub
green "Diable systemd-resolved dns stub"
sudo mkdir -p /etc/systemd/resolved.conf.d/
echo "[Resolve]
DNSStubListener=no" | sudo tee /etc/systemd/resolved.conf.d/disable-DNSStub.conf > /dev/null
sudo systemctl restart systemd-resolved

# Disable NetworkManager dns
green "Disable NetworkManager DNS Resolver"
echo "[main]
dns=none" | sudo tee /etc/NetworkManager/conf.d/disable-dns.conf > /dev/null
sudo systemctl restart NetworkManager

# Install and configure local dns
green "Checking for dnsmasq"
if ! [ -x "$(command -v dnsmasq)" ]; then
    sudo apt install dnsmasq
fi

# Disable resolvconf dnsmasq override
sudo sed -i '/#IGNORE_RESOLVCONF=yes/s/^#//g' /etc/default/dnsmasq

# Configure dnsmasq to respond to the test TLD
green "Configuring .test TLD"
if grep -q 'address=/test/10.254.254.1' "/etc/dnsmasq.conf"; then
    green "Test domain is already configured, skipping..."
else
    sudo echo 'address=/test/10.254.254.1' | sudo tee -a /etc/dnsmasq.conf > /dev/null
fi
# dnsmasq should only resolve its domains
#sudo sed -i '/#no-resolv/s/^#//g' /etc/dnsmasq.conf
sudo systemctl restart dnsmasq

# Add dnsmasq to the list of dns servers
# See: https://unix.stackexchange.com/questions/385888/how-to-reorder-etc-resolv-conf-at-load-time
# green "Configuring MacOS to resolve .test TLD via dnsmasq"
if grep -q 'prepend domain-name-servers 10.254.254.1;' "/etc/dhcp/dhclient.conf"; then
    green "DNSMasq already added to dns servers, skipping..."
else
    sudo echo 'prepend domain-name-servers 10.254.254.1;' | sudo tee -a /etc/dhcp/dhclient.conf > /dev/null
fi

# Point Docker to local dns
# See https://docs.docker.com/config/daemon/
green "Point Docker at dnsmasq to resolve domains from inside docker containers"
sudo mkdir -p /etc/docker/
if [ ! -f /etc/docker/daemon.json ]; then
    sudo touch /etc/docker/daemon.json
    echo '{}' | sudo tee /etc/docker/daemon.json > /dev/null
fi
# host.docker.internal
# 10.254.254.1
# 172.17.0.1
jq '. + {"dns":["10.254.254.1"]}' /etc/docker/daemon.json | sudo sponge /etc/docker/daemon.json
sudo systemctl restart docker

green "Setup is complete!"
