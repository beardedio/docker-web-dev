# Docker Web Development

Web development using docker, dns setup, and a reverse proxy allows for easy setup
and deployment of multiple web development projects at the same time.


## Features
* Automatically setup and manage domains for containers
* Domain access between containers and also via the host

## Supported Operating Systems
* Ubuntu 18.04+
* MacOS 10.12+
* Windows 10+


## Setup

1) Run a setup script to setup the dns resolver and ip loopback
  * For ubuntu use the [setup-ubuntu.sh](bin/setup-ubuntu.sh) script
  * For MacOS use the [setup-mac.sh](bin/setup-mac.sh) script
  * For Windows user the [setup-win.ps1](bin/setup-win.ps1) script (powershell) (NOT YET FINISHED)
2) Run the reverse proxy in docker
```
docker run -d --name=nginx-proxy --restart=always --network proxy -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy
```

## How it works
This configuration uses a combination of a local DNS server, a reverse proxy, and
docker.

### local DNS server
This resolves a wild card TLD like `*.test`. This allows to automatically use any
domain ending in .test to point to the reverse proxy for proper routing.

### reverse proxy
It will register docker containers automatically and route traffic by domain to
the correct container.

The one used in this setup is [docker-proxy](https://github.com/beardedio/docker-proxy)
